import numpy as np
import os.path
from data.base_dataset import BaseDataset, get_transform
from data.image_folder import make_dataset
from PIL import Image
import random
import util.util as util

class StyleTransferDataset(BaseDataset):
    """
        This dataset class is the implementaiton for https://arxiv.org/pdf/2203.02069.pdf
        It loads weekly-paired images in patches in each batch 

        It requires two directories to host training images from domain A '/path/to/data/trainA'
        and from domain B '/path/to/data/trainB' respectively.
        You can train the model with the dataset flag '--dataroot /path/to/data'.
        Similarly, you need to prepare two directories:
        '/path/to/data/testA' and '/path/to/data/testB' during test time.

    """

    def __init__(self, opt):
        """Initialize this dataset class.

        Parameters:
            opt (Option class) -- stores all the experiment flags; needs to be a subclass of BaseOptions
        """
        super().__init__(opt)

        self.dir_A = os.path.join(opt.dataroot, 'trainA')  # create a path '/path/to/data/trainA'
        self.dir_B = os.path.join(opt.dataroot, 'trainB')  # create a path '/path/to/data/trainB'

        if opt.phase == "val" or opt.phase == "test":
            self.dir_A = os.path.join(opt.dataroot, "valA")
            self.dir_B = os.path.join(opt.dataroot, "valB")


        if os.path.exists(self.dir_A) and os.path.exists(self.dir_B):
            self.A_paths = sorted(make_dataset(self.dir_A, opt.max_dataset_size))   # load images from '/path/to/data/trainA'
            self.B_paths = sorted(make_dataset(self.dir_B, opt.max_dataset_size))    # load images from '/path/to/data/trainB'
        self.A_size = len(self.A_paths)  # get the size of dataset A
        self.B_size = len(self.B_paths)  # get the size of dataset B

        # assert self.A_size == self.B_size,\
        #     "StyleTransferDataset class should be used with same number of images in each domain"


        # While the crop locations are randomized, the negative samples should
        # not come from the same location. To do this, we precompute the
        # crop locations with no repetition.
        self.patch_indices_A = list(range(len(self)))
        random.shuffle(self.patch_indices_A)
        self.patch_indices_B = list(range(len(self)))
        random.shuffle(self.patch_indices_B)


    def __getitem__(self, index):

        if self.opt.serial_batches: 
            index_B = index % self.B_size
        else: # randomized index for target domain to avoid fixed paires
            index_B = random.randint(0, self.B_size-1)


        A_path = self.A_paths[index % self.A_size]
        B_path = self.B_paths[index_B]

        A_img = Image.open(A_path).convert('RGB')
        B_img = Image.open(B_path).convert('RGB')

        # apply image transformation
        if self.opt.phase == "train":
            param = {'patch_index': random.choice(self.patch_indices_A),
                     'longside': random.randint(256,self.opt.load_size),
                     'flip': random.random() > 0.5}

            transform_A = get_transform(self.opt, grayscale=(self.opt.input_nc == 1), params=param, method=Image.BILINEAR)
            A = transform_A(A_img)

            param = {
                     'patch_index': random.choice(self.patch_indices_B),
                     'longside': random.randint(256,self.opt.load_size),
                     'flip': random.random() > 0.5}
            transform_B = get_transform(self.opt, params=param, grayscale=(self.opt.input_nc == 1), method=Image.BILINEAR)
            B = transform_B(B_img)
        else:
            temp_preprocess = self.opt.preprocess
            temp_load_size = self.opt.load_size
            self.opt.load_size = self.opt.load_size_test
            self.opt.preprocess = self.opt.preprocess_test
            transform = get_transform(self.opt, grayscale=(self.opt.input_nc == 1), method=Image.BILINEAR)
            A = transform(A_img)
            B = transform(B_img)
            self.opt.preprocess = temp_preprocess
            self.opt.load_size = temp_load_size

        return {'A': A, 'B': B, 'A_paths': A_path, 'B_paths': B_path}


    def __len__(self):
        """ Return the total number of images in the dataset

        """
        return max(self.A_size, self.B_size)